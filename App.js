import React from 'react';
import { SafeAreaView, StyleSheet, Platform, Image, Text, View, ScrollView, FlatList, Component } from 'react-native';
import update from 'react-addons-update';
import arrayDiff from 'simple-array-diff';

import { Header, Icon, ListItem } from 'react-native-elements';

import { ifIphoneX } from 'react-native-iphone-x-helper'

import arraySort from 'array-sort';

import Drawer from 'react-native-drawer';

import firebase from 'react-native-firebase';

const Banner = firebase.admob.Banner;

export default class SpiceRack extends React.PureComponent {
    _keyExtractor = (spice, index) => spice.post_name;
    _catKeyExtractor = (category, index) => category.slug;

    constructor() {
        super();
        this.state = {
            allSpices: [],
            categories: [],
            selectedCategory: null,
            selectedCategorySpices: [],
            isAuthenticated: false,
            collectedSpices: [],
            user: null
        };
    }

    componentWillMount() {
        this.ref = firebase.database().ref('items');
        this.ref.on('value', this.handleSpices);

        this.refCat = firebase.database().ref('category_terms');
        this.refCat.on('value', this.handleCategories);
    }

    componentDidMount() {
        var self = this;
        firebase.auth().signInAnonymouslyAndRetrieveData().then(() => {
            this.setState({isAuthenticated: true, user: firebase.auth().currentUser});
            firebase.database().ref('collections/'+firebase.auth().currentUser.uid).on('value', self.handleSelectedSpices);
        });
    }

    componentWillUpdate(nextProps, nextState) {
        if ( nextState.selectedCategory !== this.state.selectedCategory ) {
            this.filterSpices(nextState.selectedCategory);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        var self = this;
        if ( this.state.isAuthenticated && this.state.collectedSpices.length !== prevState.collectedSpices.length ) {
            if ( arrayDiff(prevState.collectedSpices, this.state.collectedSpices).added.length > 0 ) {
                arrayDiff(prevState.collectedSpices, this.state.collectedSpices).added.forEach(function (value) {
                    firebase.database().ref('collections/'+self.state.user.uid).child(value).set(true);
                });
            } else if ( arrayDiff(prevState.collectedSpices, this.state.collectedSpices).removed.length > 0 ) {
                arrayDiff(prevState.collectedSpices, this.state.collectedSpices).removed.forEach(function (value) {
                    firebase.database().ref('collections/'+self.state.user.uid).child(value).remove();
                });
            }
        }
    }

    handleCollectedSpices = (snapshot) => {
        var locallyCollectedSpices = this.state.locallyCollectedSpices;
        var a = snapshot.val();

        this.state.locallyCollectedSpices.map((y) => {
            if ( !a.child(y) ) {
                a.child(y).set(true);
            }
        });

        a.map((child) => {
            if ( this.state.locallyCollectedSpices.indexOf(child) == -1 ) {
                a.child(child).remove();
            }
        });
    }

    handleSelectedSpices = (snapshot) => {
        var array_keys = new Array();
        var a = snapshot.val();

        for (var key in a) {
            array_keys.push(parseInt(key));
        }

        this.setState({collectedSpices: array_keys});
    }

    handleSpices = (snapshot) => {
        var array_keys = new Array();
        var array_values = new Array();
        var a = snapshot.val();

        for (var key in a) {
            array_keys.push(key);
            array_values.push(a[key]);
        }

        this.setState({allSpices: arraySort(array_values, 'post_name'), selectedCategorySpices: arraySort(array_values, 'post_name')});
    }

    handleCategories = (snapshot) => {
        var array_keys = new Array();
        var array_values = new Array();
        var a = snapshot.val();

        for (var key in a) {
            array_keys.push(key);
            array_values.push(a[key]);
        }

        this.setState({categories: arraySort(array_values, 'slug')});
    }

    getSpiceCategory = (id) => {
        var categories = this.state.categories;
        var spiceCategory;
        var id = id;

        categories.map(function(category, index) {
            if ( category.slug == id ) {
                spiceCategory = category.name.replace('&amp;', '&');
            }
        });
        return spiceCategory;
    }

    filterSpices = (category) => {
        var allSpices = this.state.allSpices;
        var array_keys = new Array();
        var array_values = new Array();

        for (var key in allSpices) {
            array_keys.push(key);
            if ( allSpices[key]['sections'].includes(category) ) {
                array_values.push(allSpices[key]);
            }
        }

        if ( category ) {
            this.setState({selectedCategorySpices: arraySort(array_values, 'post_name')});
        } else {
            this.setState({selectedCategorySpices: allSpices});
        }
        this.closeDrawer();
    }

    _renderItem = ({item}) => {
        return <ListItem
            title={item.title}
            subtitle={this.getSpiceCategory(item.sections[0])}
            rightIcon={ this.state.collectedSpices.indexOf(item.id) !== -1 ? {name: 'check', color: '#444'} : {name: 'check', color: '#eee'}}
            onPressRightIcon={() => {
                if ( this.state.collectedSpices.indexOf(item.id) > -1 ) {
                    var index = this.state.collectedSpices.indexOf(item.id);
                    this.setState(prevState => ({
                        collectedSpices: update(prevState.collectedSpices, {$splice: [[index, 1]]})
                    }));
                } else {
                    this.setState(prevState => ({
                        collectedSpices: prevState.collectedSpices.concat(item.id)
                    }));
                }
            }}
        />
    }

    _renderCategory = ({item}) => (
        <ListItem
            containerStyle={{borderBottomColor: '#b63631'}}
            title={item.name.replace('&amp;', '&')}
            hideChevron={ this.state.selectedCategory == item.slug ? false : true}
            titleStyle={ this.state.selectedCategory == item.slug ? {fontWeight: 'bold', color: '#fff'} : {color: '#fff'}}
            rightIcon={ this.state.selectedCategory == item.slug ? <Icon name={'check'} size={20} color={"#ffffff"} /> : {}}
            underlayColor={'#971712'}
            onPress={() => {
                var newSelectedCategory = this.state.selectedCategory == item.slug ? null : item.slug;
                this.setState({selectedCategory: newSelectedCategory});
            }}
        />
    )

    closeDrawer = () => {
        this._drawer.close()
    };

    openDrawer = () => {
        this._drawer.open()
    };

    SpiceList = () => {
        if (this.state.selectedCategorySpices.length) {
            if ( Platform.OS === 'ios' ) {
                return <FlatList
                            data={this.state.selectedCategorySpices}
                            extraData={this.state}
                            keyExtractor={this._keyExtractor}
                            renderItem={this._renderItem} />;
            } else {
                return <View>
                    <FlatList
                        data={this.state.selectedCategorySpices}
                        extraData={this.state}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem} />
                    <Banner
                       bannerSize="smartBannerPortrait"
                       unitID="ca-app-pub-4501643307592784/1556704039"
                       testDeviceID="EMULATOR"
                       didFailToReceiveAdWithError={this.bannerError} />
                   </View>;
            }
        }
        return <Text style={styles.emptyViewText}>There are no items under that category.</Text>;
    }

    render() {
        const SpiceList = this.SpiceList();
        return (
            <Drawer
                content={<ScrollView
                    style={styles.drawerStyles}>
                    <FlatList
                        ListHeaderComponent={<ListItem
                            title={'Categories'}
                            hideChevron={true}
                            containerStyle={{borderBottomColor: '#b63631'}}
                            titleStyle={styles.headerStyles}
                        />}
                        data={this.state.categories}
                        extraData={this.state}
                        keyExtractor={this._catKeyExtractor}
                        renderItem={this._renderCategory}
                    />

                    <Image
                        style={{width: 200, height: 200, marginTop: 40, marginLeft: 'auto', marginRight: 'auto', opacity: .1}}
                        source={require('./assets/logo-200.png')}
                    />
                </ScrollView>}
                ref={(ref) => this._drawer = ref}
                openDrawerOffset={100}
                styles={drawerStyles}
                tapToClose={true}
            >
                <View style={styles.container}>
                    <Header
                        outerContainerStyles={styles.headerContainerStyles}
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={<Icon name='filter-list' color='#fff' underlayColor='transparent' onPress={() => { this.openDrawer() }} />}
                        centerComponent={<Image
                            style={{width: 87, height: 20}}
                            source={require('./assets/title.png')}
                        />}
                        backgroundColor={'#971712'}
                    />

                    <ScrollView style={{height: '100%', flex: 1,
                        ...ifIphoneX({
                            marginBottom: 22
                        })
                    }}>
                        {SpiceList}
                    </ScrollView>
                </View>
            </Drawer>
        );
    }
}

const drawerStyles = {
  drawer: { borderRightColor: "#b63631", borderRightWidth: 1, shadowColor: '#ccc', shadowOpacity: 0.8, shadowRadius: 3 }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: '100%'
    },
    logo: {
        height: 80,
        marginBottom: 16,
        width: 80,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    modules: {
        margin: 20,
    },
    modulesHeader: {
        fontSize: 16,
        marginBottom: 8,
    },
    module: {
        fontSize: 14,
        marginTop: 4,
        textAlign: 'center',
    },
    emptyViewText: {
        textAlign: 'center',
        fontWeight: 'bold',
        paddingTop: 20
    },
    headerStyles: {
        fontWeight: 'bold',
        color: 'white',
        fontSize: 18,
        paddingTop: 8,
        ...ifIphoneX({
            paddingTop: 15
        }),
        ...Platform.select({
            android: {
                paddingTop: 0
            }
        })
    },
    headerContainerStyles: {
        ...ifIphoneX({
            height: 80
        }),
        ...Platform.select({
            android: {
                height: 55,
            }
        })
    },
    drawerStyles: {
        backgroundColor: '#971712',
        paddingTop: 20,
        ...Platform.select({
            android: {
                paddingTop: 10,
            }
        })
    }
});
